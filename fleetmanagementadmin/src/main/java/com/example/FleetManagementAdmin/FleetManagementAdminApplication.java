package com.example.FleetManagementAdmin;

import de.codecentric.boot.admin.server.config.EnableAdminServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableAdminServer
public class FleetManagementAdminApplication {

	public static void main(String[] args) {
		SpringApplication.run(FleetManagementAdminApplication.class, args);
	}

}
