# FleetManagement

For run this app please clone this repository, after that in the main folder please run below command :<br>
`docker-compose build` or 
`docker-compose up -d`

can reach to the main app by <br>
`http://localhost:8080/swagger-ui.html#/`

and reach to the admin section by <br>
`http://localhost:8090/applications`

by mongo-express can see the mongo database and data's with <br>
`http://localhost:8081/`
