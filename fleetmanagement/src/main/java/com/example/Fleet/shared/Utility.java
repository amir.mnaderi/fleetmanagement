package com.example.Fleet.shared;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilderFactory;
import java.io.StringReader;

public class Utility {

    public static boolean isNullOrEmpty(String str) {
        if (str != null && !str.isEmpty())
            return false;
        return true;
    }

    public static class stringType {
        public static boolean isValidJSON(String message) {
            try {
                new JSONObject(message);
            } catch (JSONException ex) {
                try {
                    new JSONArray(message);
                } catch (JSONException ex1) {
                    return false;
                }
            }
            return true;
        }

        public static boolean isValidXML(String message) {
            try {
                DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new InputSource(new StringReader(message)));
                return true;
            } catch (Exception e) {
                return false;
            }

        }
    }
}