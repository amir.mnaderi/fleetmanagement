package com.example.Fleet.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@AllArgsConstructor
@Data
@Document
@Getter
@Setter
public class Vehicle {
    @Id
    private String _Id;
    private String Name;
    private String IVName;
    @Indexed(unique = true)
    private String plateNumber;
    private List<VehicleProperties> Properties;

    public Vehicle() {
    }
}
