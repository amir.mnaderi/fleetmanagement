package com.example.Fleet.model;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Setter
@Getter
public class VehicleProperties {
    private String Property;
    private String Value;

    public VehicleProperties() {
    }

}
