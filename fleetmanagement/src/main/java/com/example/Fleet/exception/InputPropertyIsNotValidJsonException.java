package com.example.Fleet.exception;

public class InputPropertyIsNotValidJsonException extends Exception {
    @Override
    public String getMessage() {
        return "The Input string was not a valid Json";
    }
}
