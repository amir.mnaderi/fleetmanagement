package com.example.Fleet.exception;

public class VehicleIsNotFoundException extends Throwable {
    @Override
    public String getMessage() {
        return "Vehicle is Not Found!";
    }
}
