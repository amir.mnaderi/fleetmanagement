package com.example.Fleet.exception;

public class InputVehicleValidationException extends Exception {
    private final String message;
    public InputVehicleValidationException(String message) {
        this.message=message;
    }
    @Override
    public String getMessage() {
        return message;
    }
}
