package com.example.Fleet.exception;

public class InputPropertyIsNotValidXMLException extends Exception {
    @Override
    public String getMessage() {
        return "The Input string was not a valid XML";
    }
}
