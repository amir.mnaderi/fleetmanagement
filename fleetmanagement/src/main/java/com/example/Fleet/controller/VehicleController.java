package com.example.Fleet.controller;

import com.example.Fleet.InputModels.VehicleInputModel;
import com.example.Fleet.exception.VehicleIsNotFoundException;
import com.example.Fleet.service.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

@RestController
@RequestMapping("/api/v1/Vehicles")
public class VehicleController {
    @Autowired
    private VehicleService vehicleService;

    @GetMapping()
    public ResponseEntity<?> getVehicleService() {

        HashMap<String, Object> map = new HashMap<>();
        HttpStatus httpStatus = HttpStatus.OK;
        try {
            map.put("data", vehicleService.getAllVehicles());
            map.put("error", null);
        } catch (Exception ex) {
            map.put("data", null);
            map.put("error", ex.getMessage());
            httpStatus = HttpStatus.EXPECTATION_FAILED;
        }
        return new ResponseEntity<Object>(map, httpStatus);

    }

    @PostMapping()
    public ResponseEntity<?> saveVehicle(@RequestBody VehicleInputModel vehicles,
                                         String Properties) throws Exception {

        HashMap<String, Object> map = new HashMap<>();
        HttpStatus httpStatus = HttpStatus.OK;
        try {
            map.put("data", vehicleService.addVehicle(vehicles, Properties));
            map.put("error", null);
        } catch (Exception ex) {
            map.put("data", null);
            map.put("error", ex.getMessage());
            httpStatus = HttpStatus.EXPECTATION_FAILED;
        }
        return new ResponseEntity<Object>(map, httpStatus);
    }

    @DeleteMapping(value = "{vehicleId}")
    public ResponseEntity<?> deleteVehicle(@PathVariable(value = "vehicleId") String vehicleId) throws Exception {
        HashMap<String, Object> map = new HashMap<>();
        HttpStatus httpStatus = HttpStatus.OK;
        try {
            map.put("data", vehicleService.deleteVehicle(vehicleId));
            map.put("error", null);
        } catch (Exception | VehicleIsNotFoundException ex) {
            map.put("data", null);
            map.put("error", ex.getMessage());
            httpStatus = HttpStatus.EXPECTATION_FAILED;
        }
        return new ResponseEntity<Object>(map, httpStatus);
    }

    @PostMapping(value = "/getByPlateNumbers/{plateNumber}")
    public ResponseEntity<?> getVehicleByPlateNumber(@PathVariable(value = "plateNumber") String plateNumber) throws Exception {
        HashMap<String, Object> map = new HashMap<>();
        HttpStatus httpStatus = HttpStatus.OK;
        try {
            map.put("data", vehicleService.getVehicleByPlateNumber(plateNumber));
            map.put("error", null);
        } catch (Exception ex) {
            map.put("data", null);
            map.put("error", ex.getMessage());
            httpStatus = HttpStatus.EXPECTATION_FAILED;
        }
        return new ResponseEntity<Object>(map, httpStatus);
    }
}
