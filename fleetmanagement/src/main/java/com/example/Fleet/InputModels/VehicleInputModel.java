package com.example.Fleet.InputModels;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;


@AllArgsConstructor
@Getter
@Setter
public class VehicleInputModel {
    private String Name;
    private String IVName;
    private String PlateNumber;
    public VehicleInputModel(){}
}
