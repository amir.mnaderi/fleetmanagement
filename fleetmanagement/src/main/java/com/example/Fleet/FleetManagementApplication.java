package com.example.Fleet;

import com.example.Fleet.model.VehicleProperties;
import com.example.Fleet.model.Vehicle;
import com.example.Fleet.repository.VehiclesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.List;

@SpringBootApplication
public class FleetManagementApplication {

    @Autowired
    MongoTemplate mongoTemplate;

    public static void main(String[] args) {
        SpringApplication.run(FleetManagementApplication.class, args);
    }

//    @Bean
//    CommandLineRunner runner(VehiclesRepository repository) {
//        return args -> {
//            String plate = "12A123123";
//            Vehicle vehicle = new Vehicle();
//            vehicle.setName("BMW");
//            vehicle.setIVName("121231");
//            vehicle.setPlateNumber(plate);
//            vehicle.setProperties(
//                    List.of(
//                            new VehicleProperties("color", "green"),
//                            new VehicleProperties("wheelNumber", "4"))
//            );
//
//            Query query = new Query(new Criteria("PlateNumber").is(plate));
//            Vehicle templateOne = mongoTemplate.findOne(query, Vehicle.class);
//            if (templateOne == null)
//                repository.insert(vehicle);
//        };
//
//    }
}
