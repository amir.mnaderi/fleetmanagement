package com.example.Fleet.service;

import com.example.Fleet.InputModels.VehicleInputModel;
import com.example.Fleet.exception.InputPropertyIsNotValidException;
import com.example.Fleet.exception.InputPropertyIsNotValidJsonException;
import com.example.Fleet.exception.InputPropertyIsNotValidXMLException;
import com.example.Fleet.exception.VehicleIsNotFoundException;
import com.example.Fleet.repository.VehiclesRepository;
import com.example.Fleet.model.VehicleProperties;
import com.example.Fleet.model.Vehicle;
import com.example.Fleet.shared.Utility;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class VehicleService implements VehicleServiceInterface {

    @Autowired
    private VehiclesRepository repository;
    @Autowired
    private VehicleValidationService vehiclesValidation;

    public List<Vehicle> getAllVehicles() {
        return repository.findAll();
    }

    public Optional<Vehicle> getAllVehicleByVehicleId(String vehicleId) {
        Optional<Vehicle> vehicles = repository.findById(vehicleId);
        return vehicles;
    }

    public Vehicle addVehicle(VehicleInputModel vehicleInputModel, String properties) throws Exception {
        Vehicle vehicle;
        try {
            vehicle = VehicleMapping.Map(vehicleInputModel);

            if (vehicle == null)
                throw new Exception("Vehicle Information was not prepare");

            vehiclesValidation.validate(vehicle);

            vehicle.setProperties(getVehicleProperties(properties));
            repository.insert(vehicle);

        } catch (JSONException | InputPropertyIsNotValidException | InputPropertyIsNotValidJsonException ex) {
            throw new Exception(ex.getMessage());
        }
        return vehicle;
    }


    private List<VehicleProperties> getVehicleProperties(String properties) throws JsonProcessingException, InputPropertyIsNotValidJsonException, InputPropertyIsNotValidXMLException, InputPropertyIsNotValidException {
        List<VehicleProperties> property = null;

        if (Utility.stringType.isValidJSON(properties)) {
            property = getVehiclePropertiesFromJSON(properties);
        }

        if (Utility.stringType.isValidXML(properties)) {
            property = getVehiclePropertiesFromXml(properties);
        }

        if (properties == null)
            throw new InputPropertyIsNotValidException();
        return property;
    }


    private List<VehicleProperties> getVehiclePropertiesFromXml(String properties) throws JsonProcessingException, InputPropertyIsNotValidJsonException, InputPropertyIsNotValidXMLException {
        List<VehicleProperties> vehicleProperties = null;
        String jsonPrettyPrintString = null;
        try {
            JSONObject xmlJSONObj = XML.toJSONObject(properties);
            jsonPrettyPrintString = xmlJSONObj.getJSONObject("root").get("row").toString();
        } catch (Exception ex) {
            throw new InputPropertyIsNotValidXMLException();
        }
        ObjectMapper objectMapper = new ObjectMapper();
        vehicleProperties = objectMapper.readValue(jsonPrettyPrintString, new TypeReference<List<VehicleProperties>>() {
        });


        return vehicleProperties;
    }

    private List<VehicleProperties> getVehiclePropertiesFromJSON(String properties) throws
            JsonProcessingException, InputPropertyIsNotValidJsonException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(properties, new TypeReference<List<VehicleProperties>>() {
        });
    }

    public String deleteVehicle(String vehicleId) throws VehicleIsNotFoundException {
        Optional<Vehicle> vehicle = this.getAllVehicleByVehicleId(vehicleId);
        if (vehicle.isEmpty())
            throw new VehicleIsNotFoundException();

        repository.delete(vehicle.get());
        return vehicle.get().get_Id();
    }

    public String deleteVehicleByPlateNumber(String plateNumber) throws VehicleIsNotFoundException {
        List<Vehicle> vehicles = this.getVehicleByPlateNumber(plateNumber);

        if (vehicles.isEmpty())
            throw new VehicleIsNotFoundException();

        repository.delete(vehicles.get(0));
        return vehicles.get(0).get_Id();
    }

    public List<Vehicle> getVehicleByPlateNumber(String plateNumber) {
        return repository.findAllByPlateNumber(plateNumber);
    }
}
