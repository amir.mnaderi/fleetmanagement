package com.example.Fleet.service;

import com.example.Fleet.InputModels.VehicleInputModel;
import com.example.Fleet.exception.VehicleIsNotFoundException;
import com.example.Fleet.model.Vehicle;

import java.util.List;
import java.util.Optional;

public interface VehicleServiceInterface {

    List<Vehicle> getAllVehicles();

    Optional<Vehicle> getAllVehicleByVehicleId(String vehicleId);

    Vehicle addVehicle(VehicleInputModel vehicle, String properties) throws Exception;

    String deleteVehicle(String vehicleId) throws VehicleIsNotFoundException;
}
