package com.example.Fleet.service;

import com.example.Fleet.exception.InputVehicleValidationException;
import com.example.Fleet.model.Vehicle;
import com.example.Fleet.shared.Utility;
import org.springframework.stereotype.Service;

@Service
public class VehicleValidationService implements VehicleValidationInterface {

    @Override
    public void validate(Vehicle vehicle) throws InputVehicleValidationException {
        if (vehicle != null) {
            if (Utility.isNullOrEmpty(vehicle.getName()))
                throw new InputVehicleValidationException("Vehicle Name should not NUll or Empty");
            if (Utility.isNullOrEmpty(vehicle.getPlateNumber()))
                throw new InputVehicleValidationException("Vehicle PlateNumber should not NUll or Empty");
        }
    }
}
