package com.example.Fleet.service;

import com.example.Fleet.exception.InputVehicleValidationException;
import com.example.Fleet.model.Vehicle;

public interface VehicleValidationInterface {
    void validate(Vehicle vehicle) throws InputVehicleValidationException;
}
