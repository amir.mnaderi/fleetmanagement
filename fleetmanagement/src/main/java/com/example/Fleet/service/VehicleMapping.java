package com.example.Fleet.service;

import com.example.Fleet.InputModels.VehicleInputModel;
import com.example.Fleet.model.Vehicle;

public class VehicleMapping {
    public static Vehicle Map(VehicleInputModel vehicle) {
        if (vehicle != null) {
            Vehicle vehicles = new Vehicle();
            vehicles.setName(vehicle.getName());
            vehicles.setPlateNumber(vehicle.getPlateNumber());
            vehicles.setIVName(vehicle.getIVName());
            return vehicles;
        }
        return null;
    }
}
