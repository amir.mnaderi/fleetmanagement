package com.example.Fleet.repository;

import com.example.Fleet.model.Vehicle;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface VehiclesRepository extends MongoRepository<Vehicle, String> {
    @Query("{'plateNumber' : ?0}")
    List<Vehicle> findAllByPlateNumber(String PlateNumber);
}
