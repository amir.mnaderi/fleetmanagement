package com.example.Fleet;

import com.example.Fleet.InputModels.VehicleInputModel;
import com.example.Fleet.model.Vehicle;
import com.example.Fleet.service.VehicleMapping;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static com.mongodb.assertions.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


@SpringBootTest
@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc
class VehicleMappingTest {

    @Test
    void shouldMapInputModelToVehicle(){
         VehicleInputModel vehicleInputModel = new VehicleInputModel();
         vehicleInputModel.setIVName("121231");
         vehicleInputModel.setName("BMW");
         vehicleInputModel.setPlateNumber("22A22323");
         String Id = "1111111111111";
         Vehicle vehicle = VehicleMapping.Map(vehicleInputModel);

        assertEquals(vehicle.getPlateNumber(),"22A22323");
        assertEquals(vehicle.getName(),"BMW");
    }

    @Test
    void shouldReturnNullWhenVehicleIsNull(){
        Vehicle vehicle = VehicleMapping.Map(null);
        Assertions.assertNull(vehicle);
    }
}