package com.example.Fleet;

import com.example.Fleet.InputModels.VehicleInputModel;
import com.example.Fleet.exception.VehicleIsNotFoundException;
import com.example.Fleet.service.VehicleService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.assertj.core.api.Assertions.assertThat;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest
@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc
class SaveVehicleEndpointIntegrationTests {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private VehicleService vehicleService;

    private final String plate = "12A222222";

    @BeforeEach
    void clearSampleData() throws VehicleIsNotFoundException {
        if (!vehicleService.getVehicleByPlateNumber(plate).isEmpty())
            vehicleService.deleteVehicleByPlateNumber(plate);
    }

    @Test
    void saveVehicleEndpoint() throws Exception, VehicleIsNotFoundException {



        VehicleInputModel vehicle = new VehicleInputModel();
        vehicle.setName("BMW");
        vehicle.setIVName("121231");
        vehicle.setPlateNumber(plate);

        vehicleService.addVehicle(vehicle, "[{\"property\":\"color\",\"value\":\"green\"},{\"property\":\"wheelNumber\",\"value\":4}]");

        mockMvc.perform(post("/api/v1/Vehicles/getByPlateNumbers/" + plate)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                .andExpect(MockMvcResultMatchers.jsonPath("data[0].plateNumber").value(plate))
                .andExpect(MockMvcResultMatchers.jsonPath("data[0].properties[0].property").value("color"))
                .andExpect(MockMvcResultMatchers.jsonPath("data[0].properties[0].value").value("green"));

        vehicleService.deleteVehicleByPlateNumber(plate);

    }
}
