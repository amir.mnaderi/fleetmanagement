package com.example.Fleet;


import com.example.Fleet.InputModels.VehicleInputModel;
import com.example.Fleet.exception.VehicleIsNotFoundException;
import com.example.Fleet.model.Vehicle;
import com.example.Fleet.repository.VehiclesRepository;
import com.example.Fleet.exception.InputVehicleValidationException;
import com.example.Fleet.service.VehicleMapping;
import com.example.Fleet.service.VehicleService;
import org.junit.jupiter.api.Test;

import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;


@SpringBootTest
@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc
public class VehicleServiceTest {

    @Autowired
    private VehicleService vehicleService;

    @MockBean
    private VehiclesRepository vehiclesRepository;

    private String plateNumber = "12A222222";


    @Test
    void shouldSaveNewVehicle() throws Exception {
        VehicleInputModel vehicleInputModel = new VehicleInputModel();
        vehicleInputModel.setName("BMW");
        vehicleInputModel.setIVName("121231");
        vehicleInputModel.setPlateNumber(plateNumber);

        Vehicle vehicle = VehicleMapping.Map(vehicleInputModel);

        doReturn(vehicle).when(vehiclesRepository).insert((Vehicle) any());

        Vehicle saveVehicle = vehicleService.addVehicle(vehicleInputModel, "[{\"property\":\"color\",\"value\":\"green\"},{\"property\":\"wheelNumber\",\"value\":4}]");

        assertEquals(saveVehicle.getName(), "BMW");
        assertEquals(saveVehicle.getPlateNumber(), plateNumber);
        assertFalse(saveVehicle.getProperties().isEmpty());
        assertEquals(saveVehicle.getProperties().get(0).getProperty(), "color");
        assertEquals(saveVehicle.getProperties().get(0).getValue(), "green");
    }

    @Test
    void shouldReturnExceptionWhenPlateNumberIsEmpty() throws Exception {
        VehicleInputModel vehicleInputModel = new VehicleInputModel();
        vehicleInputModel.setName("BMW");
        vehicleInputModel.setIVName("121231");

        Vehicle vehicle = VehicleMapping.Map(vehicleInputModel);
        doReturn(vehicle).when(vehiclesRepository).insert(vehicle);

        Exception exception = assertThrows(InputVehicleValidationException.class, () -> {
            Vehicle saveVehicle = vehicleService.addVehicle(vehicleInputModel, "[{\"property\":\"color\",\"value\":\"green\"},{\"property\":\"wheelNumber\",\"value\":4}]");
        });

    }

    @Test
    void shouldReturnExceptionWhenVehicleNameIsEmpty() throws Exception {
        VehicleInputModel vehicleInputModel = new VehicleInputModel();
        vehicleInputModel.setIVName("121231");
        vehicleInputModel.setPlateNumber("22A22323");

        Vehicle vehicle = VehicleMapping.Map(vehicleInputModel);
        doReturn(vehicle).when(vehiclesRepository).insert(vehicle);

        Exception exception = assertThrows(InputVehicleValidationException.class, () -> {
            Vehicle saveVehicle = vehicleService.addVehicle(vehicleInputModel, "[{\"property\":\"color\",\"value\":\"green\"},{\"property\":\"wheelNumber\",\"value\":4}]");
        });

    }

    @Test
    void shouldDeleteVehicleWhenVehicleExists() throws Exception, VehicleIsNotFoundException {
        VehicleInputModel vehicleInputModel = new VehicleInputModel();
        vehicleInputModel.setIVName("121231");
        vehicleInputModel.setPlateNumber("22A22323");
        String Id = "1111111111111";
        Vehicle vehicle = VehicleMapping.Map(vehicleInputModel);

        doNothing().when(vehiclesRepository).deleteById(Id);
        doReturn(Optional.of(vehicle)).when(vehiclesRepository).findById(Id);

        vehicleService.deleteVehicle(Id);
    }

    @Test
    void shouldRaiseExceptionInDeleteVehicleWhenVehicleExists() throws VehicleIsNotFoundException {
        VehicleInputModel vehicleInputModel = new VehicleInputModel();
        vehicleInputModel.setIVName("121231");
        vehicleInputModel.setPlateNumber("22A22323");
        String Id = "1111111111111";
        Vehicle vehicle = VehicleMapping.Map(vehicleInputModel);

        doNothing().when(vehiclesRepository).deleteById(Id);

        assertThrows(VehicleIsNotFoundException.class, () -> {
            vehicleService.deleteVehicle(Id);
        });
    }

    @Test
    void returnAllVehicle() {
        VehicleInputModel vehicleInputModel = new VehicleInputModel();
        vehicleInputModel.setIVName("121231");
        vehicleInputModel.setName("BMW");
        vehicleInputModel.setPlateNumber("22A22323");

        VehicleInputModel vehicleInputModel2 = new VehicleInputModel();
        vehicleInputModel2.setIVName("31212433");
        vehicleInputModel2.setName("Benz");
        vehicleInputModel2.setPlateNumber("56A23234");

        Vehicle vehicle = VehicleMapping.Map(vehicleInputModel);
        Vehicle vehicle2 = VehicleMapping.Map(vehicleInputModel2);

        List<Vehicle> vehicles = new ArrayList<>();
        vehicles.add(vehicle);
        vehicles.add(vehicle2);
        doReturn(vehicles).when(vehiclesRepository).findAll();

        List<Vehicle> allVehicles = vehicleService.getAllVehicles();
        assertFalse(allVehicles.isEmpty());
        assertEquals((long) allVehicles.size(), 2);

        assertNotNull(allVehicles.get(0));
        assertEquals(allVehicles.get(0).getName(), "BMW");
        assertEquals(allVehicles.get(0).getPlateNumber(), "22A22323");


        assertNotNull(allVehicles.get(1));
        assertEquals(allVehicles.get(1).getName(), "Benz");
        assertEquals(allVehicles.get(1).getPlateNumber(), "56A23234");


    }

    @Test
    void getVehicleByVehicleId() {

        Vehicle vehicle = new Vehicle();
        vehicle.setIVName("121231");
        vehicle.setName("BMW");
        vehicle.set_Id("1111111111");
        vehicle.setPlateNumber("22A22323");

        doReturn(Optional.of(vehicle)).when(vehiclesRepository).findById(any());

        Optional<Vehicle> vehicleOptional = vehicleService.getAllVehicleByVehicleId("1111111111");
        assertNotNull(vehicleOptional.get());
        assertEquals(vehicleOptional.get().getName(),"BMW");
        assertEquals(vehicleOptional.get().getIVName(),"121231");
        assertEquals(vehicleOptional.get().getPlateNumber(),"22A22323");

    }

}

