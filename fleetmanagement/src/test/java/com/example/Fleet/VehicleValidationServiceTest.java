package com.example.Fleet;

import com.example.Fleet.InputModels.VehicleInputModel;
import com.example.Fleet.exception.InputVehicleValidationException;
import com.example.Fleet.model.Vehicle;
import com.example.Fleet.service.VehicleMapping;
import com.example.Fleet.service.VehicleValidationService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc
class VehicleValidationServiceTest {

    @Autowired
    VehicleValidationService vehicleValidationService;

    @Test
    void shouldReturnExceptionWhenVehicleNameIsEmpty() throws InputVehicleValidationException {

        assertThrows(InputVehicleValidationException.class, () -> {
            VehicleInputModel vehicleInputModel = new VehicleInputModel();
            vehicleInputModel.setIVName("121231");
            Vehicle vehicle = VehicleMapping.Map(vehicleInputModel);
            vehicleValidationService.validate(vehicle);
        });

    }

    @Test
    void shouldReturnExceptionWhenVehiclePlateNumberIsEmpty() throws InputVehicleValidationException {

        assertThrows(InputVehicleValidationException.class, () -> {
            VehicleInputModel vehicleInputModel = new VehicleInputModel();
            vehicleInputModel.setName("BMW");
            vehicleInputModel.setIVName("121231");
            Vehicle vehicle = VehicleMapping.Map(vehicleInputModel);
            vehicleValidationService.validate(vehicle);
        });

    }

}