package com.example.Fleet;

import com.example.Fleet.exception.VehicleIsNotFoundException;
import com.example.Fleet.model.Vehicle;
import com.example.Fleet.model.VehicleProperties;
import com.example.Fleet.repository.VehiclesRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc
class VehiclesRepositoryTest {

    @Autowired
    protected VehiclesRepository vehiclesRepository;

    private final String plate = "12A123123";

    @Test
    void shouldReturnAllVehiclesWithPlateNumber() {

        Vehicle vehicle = new Vehicle();
        vehicle.setName("BMW");
        vehicle.setIVName("121231");
        vehicle.setPlateNumber(plate);
        vehicle.setProperties(
                List.of(
                        new VehicleProperties("color", "green"),
                        new VehicleProperties("wheelNumber", "4"))

        );

        vehiclesRepository.save(vehicle);

        List<Vehicle> vehicles = vehiclesRepository.findAllByPlateNumber(plate);

        assertEquals(vehicles.size(), 1);
        assertEquals(vehicles.get(0).getName(), "BMW");
        assertEquals(vehicles.get(0).getIVName(), "121231");
        assertEquals(vehicles.get(0).getPlateNumber(), plate);

        vehiclesRepository.deleteById(vehicles.get(0).get_Id());

    }
}