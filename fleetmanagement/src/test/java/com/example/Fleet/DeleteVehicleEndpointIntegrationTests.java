package com.example.Fleet;

import com.example.Fleet.InputModels.VehicleInputModel;
import com.example.Fleet.exception.VehicleIsNotFoundException;
import com.example.Fleet.model.Vehicle;
import com.example.Fleet.service.VehicleService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest
@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc
class DeleteVehicleEndpointIntegrationTests {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private VehicleService vehicleService;

    @Test
    void deleteVehicleEndpoint() throws Exception, VehicleIsNotFoundException {

        String plate = "12A222222";

        VehicleInputModel vehicle = new VehicleInputModel();
        vehicle.setName("BMW");
        vehicle.setIVName("121231");
        vehicle.setPlateNumber(plate);

        Vehicle vehicleData = vehicleService.addVehicle(vehicle, "[{\"property\":\"color\",\"value\":\"green\"},{\"property\":\"wheelNumber\",\"value\":4}]");

        mockMvc.perform(delete("/api/v1/Vehicles/" + vehicleData.get_Id())
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());


    }
}
