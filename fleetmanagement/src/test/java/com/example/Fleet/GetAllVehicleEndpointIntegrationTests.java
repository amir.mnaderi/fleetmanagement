package com.example.Fleet;

import com.example.Fleet.InputModels.VehicleInputModel;
import com.example.Fleet.exception.VehicleIsNotFoundException;
import com.example.Fleet.service.VehicleService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest
@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc
class GetAllVehicleEndpointIntegrationTests {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private VehicleService vehicleService;

    @BeforeEach
    void clearSampleData() throws VehicleIsNotFoundException {
        if (!vehicleService.getVehicleByPlateNumber("12A222222").isEmpty())
            vehicleService.deleteVehicleByPlateNumber("12A222222");
        if (!vehicleService.getVehicleByPlateNumber("11B232323").isEmpty())
            vehicleService.deleteVehicleByPlateNumber("11B232323");
    }

    @Test
    void getAllVehicleEndpoint() throws Exception, VehicleIsNotFoundException {

        VehicleInputModel vehicle1 = new VehicleInputModel();
        vehicle1.setName("BMW");
        vehicle1.setIVName("121231");
        vehicle1.setPlateNumber("12A222222");

        vehicleService.addVehicle(vehicle1, "[{\"property\":\"color\",\"value\":\"green\"},{\"property\":\"wheelNumber\",\"value\":4}]");

        VehicleInputModel vehicle2 = new VehicleInputModel();
        vehicle2.setName("BENZ");
        vehicle2.setIVName("121211");
        vehicle2.setPlateNumber("11B232323");

        vehicleService.addVehicle(vehicle2, "[{\"property\":\"color\",\"value\":\"blue\"}]");


        mockMvc.perform(get("/api/v1/Vehicles")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                .andExpect(MockMvcResultMatchers.jsonPath("data[0].plateNumber").value("12A222222"))
                .andExpect(MockMvcResultMatchers.jsonPath("data[0].properties[0].property").value("color"))
                .andExpect(MockMvcResultMatchers.jsonPath("data[0].properties[0].value").value("green"))
                .andExpect(MockMvcResultMatchers.jsonPath("data[1].plateNumber").value("11B232323"))
                .andExpect(MockMvcResultMatchers.jsonPath("data[1].properties[0].property").value("color"))
                .andExpect(MockMvcResultMatchers.jsonPath("data[1].properties[0].value").value("blue"));


        vehicleService.deleteVehicleByPlateNumber("12A222222");
        vehicleService.deleteVehicleByPlateNumber("11B232323");

    }
}
